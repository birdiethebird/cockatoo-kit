import React from 'react';

import { storiesOf } from '@storybook/react';

import { Button } from '../src/components/Button/Button';
import { INTENT } from '../src/theme/types';

storiesOf('Component|Button', module)
    .add('DEFAULT', () => <Button appearance={INTENT.DEFAULT}>DEFAULT</Button>)
    .add('PRIMARY', () => <Button appearance={INTENT.PRIMARY}>PRIMARY</Button>)
    .add('SECONDARY', () => <Button appearance={INTENT.SECONDARY}>SECONDARY</Button>)
    .add('WARNING', () => <Button appearance={INTENT.WARNING}>WARNING</Button>)
    .add('DANGER', () => <Button appearance={INTENT.DANGER}>DANGER</Button>)
    .add('LINK', () => <Button appearance={INTENT.LINK}>LINK</Button>);
