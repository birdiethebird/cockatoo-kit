import React from 'react';

import { storiesOf } from '@storybook/react';

import styled from 'styled-components';
import { Avatar } from '../src/components/Avatar/Avatar';
import { AVATAR_SIZES, GENDER } from '../src/components/Avatar/constants';

const AvatarWrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-around;
    align-items: center;
    width: 180px;
    padding: 16px;
`;

storiesOf('Component|Avatar', module)
    .add('DEFAULT - FEMALE', () => (
        <AvatarWrapper>
            <Avatar gender={GENDER.FEMALE} size={AVATAR_SIZES.LARGE}/>
            <Avatar gender={GENDER.FEMALE}/>
            <Avatar gender={GENDER.FEMALE} size={AVATAR_SIZES.SMALL}/>
        </AvatarWrapper>
        ))
    .add('DEFAULT - MALE', () => (
        <AvatarWrapper>
            <Avatar gender={GENDER.MALE} size={AVATAR_SIZES.LARGE}/>
            <Avatar gender={GENDER.MALE}/>
            <Avatar gender={GENDER.MALE} size={AVATAR_SIZES.SMALL}/>
        </AvatarWrapper>
    ));
