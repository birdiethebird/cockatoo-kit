import React, { useContext } from 'react';
import { ThemeContext } from '../../theme/ThemeContext';
import { INTENT, ITheme } from '../../theme/types';
import { StyledButton } from './styles';

interface IProps {
    appearance: INTENT;
}

export const Button: React.FC<IProps & React.ButtonHTMLAttributes<HTMLButtonElement>> =
    (props: IProps & React.ButtonHTMLAttributes<HTMLButtonElement>) => {
        const theme: ITheme = useContext<ITheme>(ThemeContext);

        return <StyledButton {...props} theme={theme}/>;
    };
