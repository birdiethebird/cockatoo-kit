import styled from 'styled-components';
import { colorMode } from '../../theme/constants';
import { INTENT, ITheme } from '../../theme/types';

export interface IButtonProps {
    appearance: INTENT;
    theme: ITheme;
}

export const StyledButton = styled.button<IButtonProps>`
    width: 100px;
    height: 35px;
    border: none;
    border-radius: 2px;
    cursor: pointer;
    background: ${(props) => props.theme.background[props.appearance].default[colorMode]};
    color: ${(props) => props.theme.color[props.appearance].default[colorMode]};
    transition: all ease-in-out 0.3s;
    box-shadow: 0px 5px 15px ${(props) => props.theme.boxShadowColor[props.appearance].default[colorMode]};
    :hover {
        background: ${(props) => props.theme.background[props.appearance].hover[colorMode]};
        box-shadow: 0px 2px 5px ${(props) => props.theme.boxShadowColor[props.appearance].hover[colorMode]};
    }
    :focus {
        outline: none;
    }
`;
