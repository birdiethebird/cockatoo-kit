import React, { useContext } from 'react';
import { ThemeContext } from '../../theme/ThemeContext';
import { ITheme } from '../../theme/types';
import { AVATAR_SIZES, GENDER } from './constants';
import { StyledAvatar } from './styles';

interface IProps {
    size?: AVATAR_SIZES;
    gender?: GENDER;
    src?: string;
}

export const Avatar: React.FC<IProps & React.HTMLAttributes<HTMLDivElement>> =
    (props: IProps & React.HTMLAttributes<HTMLDivElement>) => {
        const theme: ITheme = useContext<ITheme>(ThemeContext);

        return <StyledAvatar {...props} theme={theme}/>;
    };
