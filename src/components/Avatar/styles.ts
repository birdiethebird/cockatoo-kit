import styled from 'styled-components';
import femaleAvatarSrc from '../../img/avatar/avatar_female.svg';
import maleAvatarSrc from '../../img/avatar/avatar_male.svg';
import { INTENT, ITheme } from '../../theme/types';
import { AVATAR_SIZES, GENDER } from './constants';

export interface IAvatarProps {
    theme: ITheme;
    size?: AVATAR_SIZES;
    gender?: GENDER;
    src?: string;
}

export const StyledAvatar = styled.div<IAvatarProps>`
    width: ${(props) => (props.size || AVATAR_SIZES.MEDIUM) + 'px'};
    height: ${(props) => (props.size || AVATAR_SIZES.MEDIUM) + 'px'};
    background-color: ${(props) => props.theme.background.default.default.light};
    background-image: url(${(props) => props.src ? props.src :
                            (props.gender === GENDER.FEMALE ? femaleAvatarSrc : maleAvatarSrc)});
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
    color: ${(props) => props.theme.color.default.default.light};
    border-radius: 50%;
    cursor: pointer;
    :focus {
        border: none;
    }
`;
