export enum AVATAR_SIZES {
    SMALL = 24,
    MEDIUM = 36,
    LARGE = 48,
}

export enum GENDER {
    FEMALE,
    MALE,
}
