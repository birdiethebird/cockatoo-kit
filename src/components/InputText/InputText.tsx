import React, { useContext } from 'react';
import { ThemeContext } from '../../theme/ThemeContext';
import { INTENT, ITheme } from '../../theme/types';
import { StyledInputText } from './styles';

interface IProps {
    appearance: INTENT;
}

export const InputText: React.FC<IProps & React.InputHTMLAttributes<HTMLInputElement>> =
    (props: IProps & React.InputHTMLAttributes<HTMLInputElement>) => {
        const theme: ITheme = useContext<ITheme>(ThemeContext);

        return <StyledInputText {...props} theme={theme}/>;
    };
