import styled from 'styled-components';
import { colorMode } from '../../theme/constants';
import { INTENT, ITheme } from '../../theme/types';

export interface ITextProps {
    appearance: INTENT;
    theme: ITheme;
}

export const StyledInputText = styled.input<ITextProps>`
    background: transparent;
    border: 0;
    box-sizing: border-box;
    color: ${(props) => props.theme.color[props.appearance].default[colorMode]};
    cursor: inherit;
    font-family: inherit;
    font-size: inherit;
    min-width: 0;
    outline: none;
    width: 100%;

    [disabled] { }

    &::-ms-clear {
        display: none;
    }

    &:invalid {
        box-shadow: none;
    }

    :hover {
        background: ${(props) => props.theme.background[props.appearance].hover[colorMode]};
        box-shadow: 0px 2px 5px ${(props) => props.theme.boxShadowColor[props.appearance].hover[colorMode]};
    }
    :focus {
        outline: none;
    }
`;
