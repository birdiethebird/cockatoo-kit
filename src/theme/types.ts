import { Color } from '../globalTypes';

export type RGBColorString = string;

export interface IThemeColorState {
    light: Color | RGBColorString;
    dark?: Color | RGBColorString;
}

export interface IThemeIntent {
    default?: IThemeColorState;
    hover?: IThemeColorState;
    active?: IThemeColorState;
    disabled?: IThemeColorState;
    selected?: IThemeColorState;
    focus?: IThemeColorState;
    focusSelected?: IThemeColorState;
}

export enum INTENT {
    DEFAULT = 'default',
    PRIMARY = 'primary',
    SECONDARY = 'secondary',
    WARNING = 'warning',
    DANGER = 'danger',
    LINK = 'link',
}

export interface IThemeCategory {
    default: IThemeIntent;
    primary: IThemeIntent;
    secondary: IThemeIntent;
    warning: IThemeIntent;
    danger: IThemeIntent;
    link: IThemeIntent;
}

export interface ITheme {
    background: IThemeCategory;
    boxShadowColor: IThemeCategory;
    color: IThemeCategory;
}
