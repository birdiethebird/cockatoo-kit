enum COLOR_MODE {
    LIGHT = 'light',
    DARK = 'dark',
}

export const colorMode: COLOR_MODE = COLOR_MODE.LIGHT;

export const grid: number = 6;
