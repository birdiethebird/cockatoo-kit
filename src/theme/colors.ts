import { Color } from '../globalTypes';

interface IColorSet {
    [colorName: string]: Color;
}

export const colors: IColorSet = {
    N0: '#FFFFFF',
    // Reds
    R50: '#FFEBE6',
    R75: '#FFBDAD',
    R100: '#FF8F73',
    R200: '#FF7452',
    R300: '#FF5630',
    R400: '#DE350B',
    R500: '#BF2600',
    // Greens
    G50: '#e6f7f1',
    G75: '#99dfc5',
    G100: '#4dc79a',
    G200: '#00AF6F',
    G300: '#008c59',
    G400: '#006943',
    G500: '#005838',
    GA200: '#2EEDA7',
    GA500: '#177754',
    // Light Greens
    LG75:'#c5e3b9',
    LG100:'#a7d697',
    LG200:'#8ac874',
    LG300:'#6DBA51',
    // Gray Greens
    GY50: '#e8eaea',
    GY75: '#c6cccb',
    GY100: '#a4adab',
    GY200: '#8d9896',
    GY300: '#636a69',
    GY900: '#1B302D',
};
