import React, { createContext, useState } from 'react';
import { Color } from '../globalTypes';
import { colors } from './colors';
import { ITheme, RGBColorString } from './types';

function hex2rgba(hex: Color, alpha: number): RGBColorString {
    try {
        const r = parseInt(hex.slice(1, 3), 16);
        const g = parseInt(hex.slice(3, 5), 16);
        const b = parseInt(hex.slice(5, 7), 16);

        if (alpha) {
            return `rgba(${r},${g},${b},${alpha})`;
        }

        return `rgb(${r},${g},${b})`;
    } catch {
        return 'rgb(256, 256, 256)';
    }
}

export const baseTheme: ITheme = {
    // Default appearance
    background: {
        default: {
            default: { light: colors.GY50, dark: colors.GY300 },
            hover: { light: colors.GY50, dark: colors.GY300 },
            active: { light: colors.GY50, dark: colors.GY300 },
            disabled: { light: colors.GY50, dark: colors.GY300 },
            selected: { light: colors.GY50, dark: colors.GY300 },
            focusSelected: { light: colors.GY50, dark: colors.GY300 },
        },
        primary: {
            default: { light: colors.G200, dark: colors.G500 },
            hover: { light: colors.G200, dark: colors.G500 },
            active: { light: colors.G200, dark: colors.G500 },
            disabled: { light: colors.G200, dark: colors.G500 },
            selected: { light: colors.G200, dark: colors.G500 },
            focusSelected: { light: colors.G200, dark: colors.G500 },
        },
        secondary: {
            default: { light: colors.GA200, dark: colors.GA500 },
            hover: { light: colors.GA200, dark: colors.GA500 },
            active: { light: colors.GA200, dark: colors.GA500 },
            disabled: { light: colors.GA200, dark: colors.GA500 },
            selected: { light: colors.GA200, dark: colors.GA500 },
            focusSelected: { light: colors.GA200, dark: colors.GA500 },
        },
        warning: {
            default: { light: colors.Y300, dark: colors.Y300 },
            hover: { light: colors.Y200, dark: colors.Y200 },
            active: { light: colors.Y400, dark: colors.Y400 },
            disabled: { light: colors.N20A, dark: colors.DN70 },
            selected: { light: colors.Y400, dark: colors.Y400 },
            focusSelected: { light: colors.Y400, dark: colors.Y400 },
        },
        danger: {
            default: { light: colors.R400, dark: colors.R400 },
            hover: { light: colors.R300, dark: colors.R300 },
            active: { light: colors.R500, dark: colors.R500 },
            disabled: { light: colors.N20A, dark: colors.DN70 },
            selected: { light: colors.R500, dark: colors.R500 },
            focusSelected: { light: colors.R500, dark: colors.R500 },
        },
        link: {
            default: { light: 'none', dark: 'none' },
            selected: { light: colors.N700, dark: colors.N20 },
            hover: { light: colors.N700, dark: colors.N20 },
            focusSelected: { light: colors.N700, dark: colors.N20 },
        },
    },

    boxShadowColor: {
        default: {
            default: { light: hex2rgba(colors.GY100, 0.6), dark: hex2rgba(colors.GY300, 0.6) },
            hover: { light: hex2rgba(colors.GY100, 0.6), dark: hex2rgba(colors.GY300, 0.6) },
        },
        primary: {
            default: { light: hex2rgba(colors.G200, 0.6), dark: hex2rgba(colors.G500, 0.6) },
            hover: { light: hex2rgba(colors.G200, 0.6), dark: hex2rgba(colors.G500, 0.6) },
        },
        secondary: {
            default: { light: hex2rgba(colors.B200, 0.6), dark: colors.B75 },
            hover: {
                light: hex2rgba(colors.B200, 0.6),
                dark: colors.B75,
            },
        },
        warning: {
            default: { light: colors.Y500, dark: colors.Y500 },
            hover: { light: colors.Y500, dark: colors.Y500 },
        },
        danger: {
            default: { light: colors.R100, dark: colors.R100 },
            hover: { light: colors.R100, dark: colors.R100 },
        },
        link: {
            default: { light: hex2rgba(colors.B200, 0.6), dark: colors.B75 },
            hover: {
                light: hex2rgba(colors.B200, 0.6),
                dark: colors.B75,
            },
        },
    },

    color: {
        default: {
            default: { light: colors.N400, dark: colors.DN400 },
            active: { light: colors.B400, dark: colors.B400 },
            disabled: { light: colors.N70, dark: colors.DN30 },
            selected: { light: colors.N20, dark: colors.DN400 },
            focusSelected: { light: colors.N20, dark: colors.DN400 },
        },
        primary: {
            default: { light: colors.N0, dark: colors.GY900 },
            disabled: { light: colors.GY200, dark: colors.GY200 },
            selected: { light: colors.N20, dark: colors.DN400 },
            focusSelected: { light: colors.N20, dark: colors.DN400 },
        },
        secondary: {
            default: { light: colors.N0, dark: colors.GY900 },
            disabled: { light: colors.GY200, dark: colors.GY200 },
            selected: { light: colors.N20, dark: colors.DN400 },
            focusSelected: { light: colors.N20, dark: colors.DN400 },
        },
        warning: {
            default: { light: colors.N800, dark: colors.N800 },
            disabled: { light: colors.N70, dark: colors.DN30 },
            selected: { light: colors.N800, dark: colors.N800 },
            focusSelected: { light: colors.N800, dark: colors.N800 },
        },
        danger: {
            default: { light: colors.N0, dark: colors.N0 },
            disabled: { light: colors.N70, dark: colors.DN30 },
            selected: { light: colors.N0, dark: colors.N0 },
            focusSelected: { light: colors.N0, dark: colors.N0 },
        },
        link: {
            default: { light: colors.B400, dark: colors.B100 },
            hover: { light: colors.B300, dark: colors.B75 },
            active: { light: colors.B500, dark: colors.B200 },
            disabled: { light: colors.N70, dark: colors.DN100 },
            selected: { light: colors.N20, dark: colors.N700 },
            focusSelected: { light: colors.N20, dark: colors.N700 },
        },
    },
};

export const ThemeContext = createContext<ITheme>(baseTheme);
